import pandas as pd

path = "../Data/"
data = pd.read_csv(path + "Coffeebar_2013-2017.csv", sep=";")

#what food and drinks movies['genres'].str.contains("Animations")
list_food = []
for food in data['FOOD']:
    if food not in list_food:
        list_food.append(food)

print ("The foods sold by the coffeebar is" + (list_food))

list_drink = []
for drink in data['DRINKS']:
    if drink not in list_drink:
        list_drink.append(drink)

print ("The drinks sold by the coffeebar is" + (list_drink))